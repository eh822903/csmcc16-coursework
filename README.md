ABSTRACT
-------------------------------------------
This project is to implement a software prototype for Hadoop MapReduce Job based on passenger flight data.
The prototype can be hosted to run Hadoop MapReduce Job using hadoop-streaming.jar.
Check PDF report for a more detailed explanation.

You can find the mapper, combiner, reducer py scripts inside Solution/hadoop_scripts/

You can find the objective py scripts inside Solution

HOW TO USE?
-------------------------------------------
ONLY FOR PYTHON2.7 (AS HADOOP PYTHON AVAILABLE IS PYTHON2.7)
1. Download Solution Folder
2. Use the command below for Hadoop MapReduce Job.
3. Or use pipeline command:

        cat AComp_Passenger_data.csv | python hadoop_scripts/mapper.py | sort | python hadoop_scripts/combiner.py | sort | python hadoop_scripts/reducer.py >> output/part-00000

4. Output objectives by running with output/part-00000: 

        python objective.py >> output.txt

HADOOP COMMAND PROCEDURE
-------------------------------------------
1. Make scripts executable:

        chmod a+rx hadoop_scripts/*

2. Upload input file to HDFS:

        hadoop fs -put AComp_Passenger_data.csv

3. Remove exisiting output directory:

        hadoop fs -rm output/*
        hadoop fs -rmdir output

4. Run MapReduce Job:

        hadoop jar /usr/hdp/3.1.4.0-315/hadoop-mapreduce/hadoop-streaming-3.1.1.3.1.4.0-315.jar -files hadoop_scripts,Top30_airports_LatLong.csv -mapper "hadoop_scripts/mapper.py" -combiner "hadoop_scripts/combiner.py" -reducer "hadoop_scripts/reducer.py" -input AComp_Passenger_data.csv -output output

5. Get Job Output:

        hadoop fs -get output


MAPPER, COMBINER, REDUCER, OBJECTIVE OUTPUT
-------------------------------------------
Mapper

        cat AComp_Passenger_data.csv | python hadoop_scripts/mapper.py >> 1.mapper_output.txt

Combiner

        cat AComp_Passenger_data.csv | python hadoop_scripts/mapper.py | sort | python hadoop_scripts/combiner.py >> 2.combiner_output.txt

Reducer

        cat AComp_Passenger_data.csv | python hadoop_scripts/mapper.py | sort | python hadoop_scripts/combiner.py | sort | python hadoop_scripts/reducer.py >> 3.reducer_output.txt

Objectives

        python objective.py >> 4.objective_output.txt (with output/part-00000 as the output of reducer)

MAPREDUCE PIPELINE:
-------------------------------------------
| MAPPER

READ LINE >> DETECT & DROP NULL >> DETECT & CORRECT AIRPORT FORMAT >> MAP >> CREATE ID_POOLS


| COMBINER

LOAD FLIGHT_ID_POOL >> LOAD PASSENGER_ID_POOL >> ERROR DETECTION & CORRECTION >> COMBINE

| REDUCER

LOAD FLIGHT_ID_POOL >> LOAD PASSENGER_ID_POOL >> ERROR DETECTION & CORRECTION >> REDUCE

OBJECTIVES
-------------------------------------------
OBJECTIVE_1:

COMPARE OUTPUT KEY & AIRPORT KEY >> IF SAME -> +1 >> OUTPUT

OBJECTIVE_2:

LIST OUTPUT KEY & VALUE 

OBJECTIVE_3: >> LIST OUTPUT VALUE LENGTH

OBJECTIVE_4:

DICT PASSENGER & FLIGHT >> REMOVE DUPLICATES >> LOOP OUTPUT DICT >> GET_DISTANCE(CLASS.(2), CLASS.(3)) >> IF PASSENGER IN FLIGHT -> += GET_DISTANCE >> OUTPUT
