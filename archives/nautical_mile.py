#!/usr/bin/python
import zipimport
importer = zipimport.zipimporter('library.zip')
nautical_calculation = importer.load_module('nautical_calculation')

from nautical_calculation import get_distance

lat1 = 50
long1 = 50
lat2 = 40
long2 = 110

print(get_distance(lat1, long1, lat2, long2) * 0.621371)


