#! /usr/bin/env python
"""mapper.py"""

from __future__ import print_function
# Mapper
import sys
import re
from difflib import get_close_matches
from flight_structure import FLIGHT

# Global variable
FLIGHT_ID_POOL = []
PASSENGER_ID_POOL = []

def read_archive(file_path):
    airport_list = []

    for line in open(file_path):
        line = line.strip().split(",")
        # If the line has NULL elements, skip
        result = any([re.search("^\s*$", x) for x in line])
        if result:
            continue

        # Append code to airport list
        name, code, x, y = line
        airport_list.append(code)

    return(airport_list)

# Import airport list for checking airport code
airport_list = read_archive("Top30_airports_LatLong.csv")

# Read line by line in STDIN
for line in sys.stdin:
    # Remove white Space & # Split into words by ","
    elements = line.strip().split(",")

    # Skip if the length of the line is not 6 elements
    if len(elements) != 6:
        continue
    
    # Error Detection & Correction
    # 1. NULL RECORD
    # If any of the element is empty, skip
    result = any([re.search("^\s*$", x) for x in elements])
    if result:
        print("%s\t%s" % ("NULL_RECORD_DISCARD", 1))
        continue

    # Reconstruct the line for data structure
    results = elements[-5:] + elements[:-5]
    line = "\t".join(results)
    current = FLIGHT(line)

    # Error Detection & Correction
    # 2. IATA/FAA Code Format
    if not current.FROM_AIRPORT in airport_list:
        # Fit the cloest AIPORT if it is not on the list
        current.FROM_AIRPORT = get_close_matches(current.FROM_AIRPORT, airport_list, n=1)[0]
        print("%s\t%s" % ("FROM_AIRPORT_FORMAT_ERROR", 1))

    if not current.TO_AIRPORT in airport_list:
        # Fit the cloest AIPORT if it is not on the list
        current.TO_AIRPORT = get_close_matches(current.TO_AIRPORT, airport_list, n=1)[0]
        print("%s\t%s" % ("TO_AIRPORT_FORMAT_ERROR", 1))

    # Mapper Output
    results = current.output()
    print("\t".join(results))

    # Add FLIGHT_ID & PASSENGER_ID TO THE POOL
    FLIGHT_ID_POOL.append(current.FLIGHT_ID)
    PASSENGER_ID_POOL.append(current.PASSENGER_LIST[0])

# Output ID_POOL for cross-referencing
print("0_flight_pool","\t","",sep='',end="")
print("\t".join(list(set(FLIGHT_ID_POOL))))
print("0_passenger_pool","\t","",sep='',end="")
print("\t".join(list(set(PASSENGER_ID_POOL))))