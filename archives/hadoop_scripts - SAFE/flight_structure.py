#! /usr/bin/env python

class FLIGHT:
    def __init__(self, x):
        s = None
        # If it is csv,
        if "," in x:
            s = ","
        else:
            # Otherwise, it should be tsv
            s = "\t"
            
        self.FLIGHT_ID, self.FROM_AIRPORT, self.TO_AIRPORT, self.DEPART_TIME, self.TOTAL_FLIGHT_TIME, self.PASSENGER_LIST = x.strip().split(s, 5)
        self.PASSENGER_LIST = list(set(self.PASSENGER_LIST.strip().split(s)))
        
    def output(self):
        results = [self.FLIGHT_ID, self.FROM_AIRPORT, self.TO_AIRPORT, self.DEPART_TIME, str(self.TOTAL_FLIGHT_TIME)]
        # If Passenger_List has more than 1 element, extend to results
        if len(self.PASSENGER_LIST) > 1:
            results.extend(set(self.PASSENGER_LIST))
        else: 
            # Otherwise, just add to results
            results += self.PASSENGER_LIST
        return(results)

    def __len__(self):  # For len()
        return 5 + len(self.PASSENGER_LIST)

    def passenger_list(self):   # For removing duplicates
        return(list(set(self.PASSENGER_LIST)))

