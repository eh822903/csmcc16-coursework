#!/usr/bin/python

import sys

words = list()

# Input from stdin
for line in sys.stdin:
    # Taking the first word
    word = line.strip().split()[0]
    # Append to list words
    words.append(word)

# Sort list
words.sort()

# Output to stdout
for line in words:
    print("%s\t%s" % (line, 1))
   