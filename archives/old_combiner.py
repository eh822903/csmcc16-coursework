#! /usr/bin/env python
"""combiner.py"""

from __future__ import print_function
import sys
import re
from difflib import get_close_matches
from flight_structure import FLIGHT

# Global variable
error_count = 0
current_error = None
current_error_count = 0
corrupt_count = 0
last_flight = None
last = None

FLIGHT_ID_POOL = []
PASSENGER_ID_POOL = []
    
# Read line by line from STDIN
for line in sys.stdin:
    # Remove white space
    elements = line.strip().split('\t')

    # Load Pool
    if elements[0] == '!flight_pool':
        FLIGHT_ID_POOL = elements[1:]
        continue
  
    if elements[0] == '!passenger_pool':
        PASSENGER_ID_POOL = elements[1:]
        continue

    if len(elements) == 2:
        # ERROR CODE
        error_code, error_count = elements
        
        # If error_count is not integer, skip
        try: 
            error_count = int(error_count)
        except ValueError:
            continue
        
        # Check if error_code is still current_error
        if current_error == error_code:
            current_error_count += error_count
            
        else:
            # If error_code is a new error_code, output the previous one
            if current_error:
                results = [current_error, str(current_error_count)]
                print("\t".join(results))
            
            # Set new error_code
            current_error = error_code
            current_error_count = error_count

        continue
        
    elif len(elements) != 6:
        # CORRUPTED RECORD
        corrupt_count += 1
        continue

    # FLIGHT RECORD
    # Load into Data Structure
    current = FLIGHT(line)

    # Error Detection & Correction
    # 3. Format Check
    passenger_format = re.compile("^[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]$")
    flight_format = re.compile("^[A-Z]{3}[0-9]{4}[A-Z]$")
    depart_time_format = re.compile(("^[0-9]{10}"))

    try:
        current.TOTAL_FLIGHT_TIME = int(current.TOTAL_FLIGHT_TIME)
    except ValueError:
        # If TOTAL_FLIGHT_TIME is not integer, continue
        print("%s\t%s" % ("TIME_NOT_INTEGER", 1))
        continue

    if not passenger_format.match(current.PASSENGER_LIST[0]):
        print("%s\t%s" % ("PASSENGER_FORMAT_ERROR", 1))
        # Try to find the closest match from the pool
        tmp = get_close_matches(current.PASSENGER_LIST[0], PASSENGER_ID_POOL, n=2)
        # If unable to do so, skip
        if len(tmp) <2:
            continue
        else: 
            # print("Corrected PASSENGER_LIST:", current.PASSENGER_LIST[0], "to", tmp[1])
            current.PASSENGER_LIST[0] = tmp[1]
            
    elif not flight_format.match(current.FLIGHT_ID.upper()):
        print("%s\t%s" % ("FLIGHT_FORMAT_ERROR", 1))
        # Try to find the closest match from the pool
        tmp = get_close_matches(current.FLIGHT_ID, FLIGHT_ID_POOL, n=2)
        # If unable to do so, skip
        if len(tmp) <2:
            continue
        else:
            # print("Corrected FLIGHT_ID:", current.FLIGHT_ID, "to", tmp[1])
            current.FLIGHT_ID = tmp[1]
            
    elif not depart_time_format.match(current.DEPART_TIME):
        print("%s\t%s" % ("DEPARTURE_TIME_FORMAT_ERROR", 1))
        continue
    elif current.TOTAL_FLIGHT_TIME > 9999:
        print("%s\t%s" % ("FLIGHT_TIME_FORMAT_ERROR", 1))
        continue

    # Combiner Output
    # Input should be sorted by key(flight_id) before passed to reducer

    # Check if current flight is current.FLIGHT_ID
    if last_flight != current.FLIGHT_ID:
        # If last_flight is not current.FLIGHT_ID, output the previous FLIGHT_ID
        if last_flight:
            # Output to stdout
            print("\t".join(last.output()))

        # Set new FLIGHT_ID
        # print("============================ NEW ENTRY ============================")
        last = current
        last_flight = last.FLIGHT_ID

    else:
        # If last_flight is current.FLIGHT_ID, continue adding PASSENGER_LIST
        last.PASSENGER_LIST.extend(current.PASSENGER_LIST)
        
# Output Last Flight
if last_flight == current.FLIGHT_ID:
    print("\t".join(last.output()))

# Output Last Error
if current_error == error_code:
    results = [current_error, str(current_error_count)]
    print("\t".join(results))

# Output Corrupted Record
print("%s\t%s" % ("Corrupted Record", corrupt_count))