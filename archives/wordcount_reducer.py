#!/usr/bin/python
# coding: utf-8

"""reducer.py"""

from operator import itemgetter
import sys

current_word = None
current_count1 = 0
current_count2 = 0
word = None

# Input from stdin
for line in sys.stdin:
    # Remove white space
    line = line.strip()

    # Parse the line into string word & string count
    word, count1, count2 = line.split('\t')

    # Convert string count into int count
    try:
        count1 = int(count1)
        count2 = int(count2)
    except ValueError:
        # if count is not not integer, continue
        continue

    # Input should be sorted by key(word) before passed to reducer
    # if the current_word equals the word that was previously processing, count + 1
    if current_word == word:
        current_count1 += count1
        current_count2 += count2
    else:
        # if the current_word is not NONE, the first word is done parsing -> print output
        if current_word:
            # output to stdout
            results = [current_word, str(current_count1), str(current_count2)]
            print("\t".join(results))
        
        # else, it is the first word we are parsing, set genesis current_word
        current_count1 = count1
        current_count2 = count2
        current_word = word

# print last line if it is different
if current_word == word:
    results = [current_word, str(current_count1), str(current_count2)]
    print("\t".join(results))

