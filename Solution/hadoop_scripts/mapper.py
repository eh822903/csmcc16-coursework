#! /usr/bin/env python
"""mapper.py"""

from __future__ import print_function
import sys
import re
# from flight_structure import FLIGHT
from mylib import read_archive, FLIGHT, get_levenshtein_match

# Global variable
FLIGHT_ID_POOL = []
PASSENGER_ID_POOL = []

# Import airport list for checking airport code
airport_list = read_archive("Top30_airports_LatLong.csv")

# Read line by line in STDIN
for line in sys.stdin:
    # Remove white Space & # Split into words by ","
    elements = line.strip().split(",")

    # Skip if the length of the line is not 6 elements
    if len(elements) != 6:
        continue
    
    # Error Detection & Correction
    # 1. NULL RECORD
    # If any of the element is empty, skip
    result = any([re.search("^\s*$", x) for x in elements])
    if result:
        print("%s\t%s" % ("NULL_RECORD_DISCARD", 1))
        continue

    # Reconstruct the line for data structure
    # Put Passenger ID at the last place of the list
    elements = elements[-5:] + elements[:-5]
    # Combine Flight ID & Departure Aiport Code
    results = [elements[0] + "_" + elements[1]]
    results.extend(elements[2:])
    line = "\t".join(results)
    current = FLIGHT(line)

    # Error Detection & Correction
    # 2. IATA/FAA Code Format
    if not current.FROM_AIRPORT in airport_list:
        # Fit the cloest AIPORT if it is not on the list
        current.FROM_AIRPORT = get_levenshtein_match(current.FROM_AIRPORT, airport_list)
        print("%s\t%s" % ("FROM_AIRPORT_FORMAT_ERROR", 1))

    if not current.TO_AIRPORT in airport_list:
        # Fit the cloest AIPORT if it is not on the list
        current.TO_AIRPORT = get_levenshtein_match(current.TO_AIRPORT, airport_list)
        print("%s\t%s" % ("TO_AIRPORT_FORMAT_ERROR", 1))

    # Mapper Output
    results = current.output()
    print("\t".join(results))

    # Add FLIGHT_ID & PASSENGER_ID TO THE POOL
    FLIGHT_ID_POOL.append(current.key())
    PASSENGER_ID_POOL.append(current.PASSENGER_LIST[0])

# Output ID_POOL for cross-referencing
print("0_flight_pool","\t","",sep='',end="")
print("\t".join(list(set(FLIGHT_ID_POOL))))
print("0_passenger_pool","\t","",sep='',end="")
print("\t".join(list(set(PASSENGER_ID_POOL))))