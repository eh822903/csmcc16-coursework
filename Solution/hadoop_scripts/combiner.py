#! /usr/bin/env python
"""combiner.py"""

from __future__ import print_function
import sys
import re
from mylib import error_code_check, FLIGHT, get_levenshtein_match

# Global Variable
FLIGHT_ID_POOL = []
PASSENGER_ID_POOL = []

def main():
    # Global Variable
    global FLIGHT_ID_POOL
    global PASSENGER_ID_POOL

    current_error = None
    current_error_count = 0
    corrupt_count = 0
    error_code = None
    last_flight = None
    last = None

    # Read line by line from STDIN
    for line in sys.stdin:
        # Remove white space & Split up into a list
        elements = line.strip().split('\t')

        # Load Pool
        if elements[0] == '0_flight_pool':
            FLIGHT_ID_POOL.extend(elements[1:])
            FLIGHT_ID_POOL = list(set(FLIGHT_ID_POOL))
            print(line, end="")
            continue
    
        if elements[0] == '0_passenger_pool':
            PASSENGER_ID_POOL.extend(elements[1:])
            FLIGHT_ID_POOL = list(set(FLIGHT_ID_POOL))
            print(line, end="")
            continue
        
        # Determine the line record type
        # ERROR RECORD
        if len(elements) == 2:
            error_code, current_error, current_error_count = error_code_check(elements, error_code, current_error, current_error_count)
            continue
        
        # CORRUPTED RECORD
        elif len(elements) != 5:
            corrupt_count += 1
            continue

        # FLIGHT RECORD
        # Load into Data Structure FLIGHT
        current = FLIGHT(line)

        # Error Detection & Correction
        # 3. Format Check
        format_error_code, current = format_check(current)
        if format_error_code == 1:
            continue

        # Combiner Output 
        # Check if current flight is current.key
        
        if last_flight != current.key():
            # If last_flight is not current.key, output the previous key
            if last_flight:
                # Output to stdout
                print("\t".join(last.output()))

            # Set new key
            last = current
            last_flight = last.key()

        else:
            # If last_flight is current.key, continue adding PASSENGER_LIST
            last.PASSENGER_LIST.extend(current.PASSENGER_LIST)
            
    # Output Last Flight
    if last_flight == current.key():
        print("\t".join(last.output()))

    # Output Last Error
    if current_error == error_code:
        results = [current_error, str(current_error_count)]
        print("\t".join(results))

    # Output Corrupted Record
        print("%s\t%s" % ("Corrupted Record", corrupt_count))

def format_check(x):
    global FLIGHT_ID_POOL
    global PASSENGER_ID_POOL
    passenger_format = re.compile("^[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]$")
    flight_format = re.compile("^[A-Z]{3}[0-9]{4}[A-Z]$")
    depart_time_format = re.compile(("^[0-9]{10}"))

    # Flight Time Integer Check
    try:
        x.TOTAL_FLIGHT_TIME = int(x.TOTAL_FLIGHT_TIME)
    except ValueError:
        print("%s\t%s" % ("TIME_NOT_INTEGER_DISCARD", 1))
        return 1, x
    
    # Passenger ID Format Check
    if not passenger_format.match(x.PASSENGER_LIST[0]): 
        print("%s\t%s" % ("PASSENGER_FORMAT_ERROR", 1))
        # Try to find the closest match from the pool
        tmp = get_levenshtein_match(x.PASSENGER_LIST[0], PASSENGER_ID_POOL)
        x.PASSENGER_LIST[0] = tmp

    # Flight ID Format Check     
    if not flight_format.match(x.FLIGHT_ID):
        print("%s\t%s" % ("FLIGHT_ID_FORMAT_ERROR", 1))
        # Try to find the closest match from the pool
        tmp = get_levenshtein_match(x.key(), FLIGHT_ID_POOL)
        x.FLIGHT_ID, x.FROM_AIRPORT = tmp.split("_")
    
    # Departure Time Format Check
    if not depart_time_format.match(x.DEPART_TIME):
        print("%s\t%s" % ("DEPARTURE_TIME_FORMAT_ERROR_DISCARD", 1))
        return 1, x

    # Flight Time Format Chheck
    if x.TOTAL_FLIGHT_TIME > 9999:
        print("%s\t%s" % ("FLIGHT_TIME_FORMAT_ERROR_DISCARD", 1))
        return 1, x
    
    return 0, x


main()