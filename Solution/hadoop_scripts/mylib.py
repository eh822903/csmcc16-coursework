#! /usr/bin/env python
import sys
import re
from pylev import wfi_levenshtein

# DATA STRUCTURE
class FLIGHT:
    def __init__(self, x):
        s = None
        FLIGHT_FROM = None

        # If it is csv,
        if "," in x:
            s = ","
        else:
            # Otherwise, it should be tsv
            s = "\t"
            
        FLIGHT_FROM, self.TO_AIRPORT, self.DEPART_TIME, self.TOTAL_FLIGHT_TIME, self.PASSENGER_LIST = x.strip().split(s, 4)
        self.FLIGHT_ID, self.FROM_AIRPORT = FLIGHT_FROM.split("_")
        self.PASSENGER_LIST = list(set(self.PASSENGER_LIST.strip().split(s)))
        
    def output(self):
        results = [self.key(), self.TO_AIRPORT, self.DEPART_TIME, str(self.TOTAL_FLIGHT_TIME)]
        # If Passenger_List has more than 1 element, extend to results
        if len(self.PASSENGER_LIST) > 1:
            results.extend(set(self.PASSENGER_LIST))
        else: 
            # Otherwise, just add to results
            results += self.PASSENGER_LIST
        return(results)

    def __len__(self):  # For len()
        return 5 + len(self.PASSENGER_LIST)
    
    def key(self):
        x = self.FLIGHT_ID + "_" + self.FROM_AIRPORT
        return(x)

    def passenger_list(self):   # For removing duplicates
        return(list(set(self.PASSENGER_LIST)))


def read_archive(file_path):
    airport_list = []

    for line in open(file_path):
        line = line.strip().split(",")
        # If the line has NULL elements, skip
        result = any([re.search("^\s*$", x) for x in line])
        if result:
            continue

        # Append code to airport list
        name, code, x, y = line
        airport_list.append(code)

    return(airport_list)

def error_code_check(elements, error_code, current_error, current_error_count):
    # ERROR CODE
    error_code, error_count = elements
    
    # If error_count is not integer, skip
    try: 
        error_count = int(error_count)
    except ValueError:
        return (error_code, current_error, current_error_count)
    
    # Check if error_code is still current_error
    if current_error == error_code:
        current_error_count += error_count
        
    else:
        # If error_code is a new error_code, output the previous one
        if current_error:
            results = [current_error, str(current_error_count)]
            print("\t".join(results))
        
        # Set new error_code
        current_error = error_code
        current_error_count = error_count

    return (error_code, current_error, current_error_count)

def get_levenshtein_match(target, pool):
    # Set base case
    lowest = wfi_levenshtein(target, pool[0])   # From pylev
    found = pool[0]
    if target == pool[0]:
        found = pool[1]
    
    # Finding the lowest score
    for trial in pool:
        score = wfi_levenshtein(target, trial)
        if score == 0:
            continue
        elif score < lowest:
            lowest = score
            found = trial
    
    # Output
    return found


