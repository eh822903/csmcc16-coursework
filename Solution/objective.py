#!/usr/bin/python
# coding: utf-8

"""objective.py"""

from __future__ import print_function
import sys
import datetime
import re
from nautical_calculation import get_distance
from mylib import FLIGHT, get_levenshtein_match

def main():
    # Read airport information and hadoop outputs 
    airport = read_airport("Top30_airports_LatLong.csv")
    output, error, flight_id_pool, passenger_id_pool = read_output("./output/part-00000")


    # Objective 1
    print("============================ Objective 1 ============================")
    # Number of Flights from each airport, Number of airports not used

    # Usage Dictionary
    usage = {}
    for x in sorted(airport.keys()):    # Traverse through airport list
        usage[x] = [0, 0]
        for y in output.values():       # Traverse through flight records
            if y.FROM_AIRPORT == x:     # Match From Airport code
                usage[x][0] += 1

            if y.TO_AIRPORT == x:       # Match To Airport code
                usage[x][1] += 1

    # Output
    print("Departure FROM the airport:")
    for k, v in sorted(usage.items()):
        if v[0] != 0:
            print("%s (%s) %i" % (airport[k][0], k, v[0]))

    print()
    print("ARRIVAL TO the airport:")
    for k, v in sorted(usage.items()):
        if v[1] != 0:
            print("%s (%s) %i" % (airport[k][0], k, v[1]))

    print()
    print("Following airport(s) are not in use:")
    for k, v in sorted(usage.items()):
        if v[0] == 0 and v[1] == 0:
            print("%s (%s)" % (airport[k][0], k))


    # Objective 2
    print("============================ Objective 2 ============================")
    # List of Flights, showing passenger ids, IATA/FAA codes, departure time, arrival time, flight time
    for k, v in sorted(output.items()):
        # Flight ID
        print("Flight ID:", k)

        # Passenger ID
        print("Passenger ID:", end=" ")
        for x in v.passenger_list():
            print(x, end=" ")
        print()

        # From Airport
        print("From Airport: %s (%s)" % (airport[v.FROM_AIRPORT][0], v.FROM_AIRPORT))

        # To Airport
        print("To Airport: %s (%s)" % (airport[v.TO_AIRPORT][0], v.TO_AIRPORT))

        # Departure Time
        time = datetime.datetime.fromtimestamp(int(v.DEPART_TIME)).strftime('%H:%M:%S')
        print("Departure Time:", time)

        # Arrival Time
        time = datetime.datetime.fromtimestamp(int(v.DEPART_TIME)+int(v.TOTAL_FLIGHT_TIME)*60).strftime('%H:%M:%S')
        print("Arrival Time:", time)

        # Flight Time
        print("Flight Time (min):", v.TOTAL_FLIGHT_TIME)

        print("=====================================================================")


    # Objective 3
    print("============================ Objective 3 ============================")
    # Calculate the number of passengers on each flight
    count = 0
    for k, v in sorted(output.items()):
        print("Flight", k, "has", len(v.passenger_list()), "passenger(s).")
        count += len(v.passenger_list())


    # Objective 4
    print("============================ Objective 4 ============================")
    # Calculate the line-of-sight (nautical) miles for each flight and, 
    # the total ravelled by each passenger and,
    # thus output the passenger having earned the highest air miles.

    # Passengers & Flight miles dictionary
    passenger_miles = {}
    flight_miles = {}

    # Format Check to format the pool
    passenger_format = re.compile("^[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]$")
    passenger_id_pool = [x for x in passenger_id_pool if passenger_format.match(x)]

    # Travser through the whole output dictinoary, flight by flight
    km2miles = 0.621371 # Conversion Rate
    for k, v in sorted(output.items()):

        # Calculate the nautical miles for each flight
        lat1, long1 = airport[v.FROM_AIRPORT][1], airport[v.FROM_AIRPORT][2]
        lat2, long2 = airport[v.TO_AIRPORT][1], airport[v.TO_AIRPORT][2]
        nm = get_distance(lat1, long1, lat2, long2) * km2miles

        # Store into flight miles dictionary
        flight_miles[k] = nm

        # If passengers on flight -> add nautical miles
        for p in v.passenger_list():
            if p in passenger_miles:
                passenger_miles[p] += nm
            else:
                passenger_miles[p] = nm

    # Output
    # flight_miles output
    print("Air miles of each flight:")
    results = {k: round(v,2) for k, v in sorted(flight_miles.items(), key=lambda item: item[1])}
    for k, v in results.items():
        print(k, v)
    print()

    # passenger_miles output
    print("Air miles of each passengers:")
    results = {k: round(v,2) for k, v in sorted(passenger_miles.items(), key=lambda item: item[1])}
    for k, v in results.items():
        print(k, v)
    print()

    # highest air miles passenger
    k = max(passenger_miles, key=passenger_miles.get)
    print("Passenger {} has the highest air miles of {:0.2f} miles.".format(k, round(passenger_miles[k], 2)))

    print("================================= END =================================")
    
    print("Error Summary:")
    for k, v in error.items():
        print(k, v)


def read_airport(file_path):
    # Airport Dictionary
    out = {}
    for line in open(file_path):
        line = line.strip().split(",")
        if len(line) == 4:
            out[line[1]] = [line[0], float(line[2]), float(line[3])]
    return(out)

def read_output(file_path):
    out1 = {}
    out2 = {}
    out3 = None
    out4 = None

    for line in open(file_path):
        # Split line into 5 elements if possible
        elements = line.strip().split("\t", 4)
        
        if len(elements) == 2:                  # If only two elements, it is error code
            if elements[0] in out2:
                out2[elements[0]] += int(elements[1])
            else:
                out2[elements[0]] = int(elements[1])
        elif elements[0] == '0_flight_pool':    # Load flight id pool
            if not out3:    
                lines = line.strip().split("\t")
                out3 = lines[1:]
            else:
                out3.extend(lines[1:])
                out3 = list(set(out3))
        elif elements[0] == '0_passenger_pool': # Load passenger id pool
            if not out4:    
                lines = line.strip().split("\t")
                out4 = lines[1:]
            else:
                out4.extend(lines[1:])
                out4 = list(set(out4))
        else:
            if elements[0] in out1:             # Load Flight records
                tmp = FLIGHT(line)
                out1[elements[0]].PASSENGER_LIST.extend(tmp.PASSENGER_LIST)

                # Remove Duplicates if any
                out1[elements[0]].PASSENGER_LIST = out1[elements[0]].passenger_list()   
            else:
                out1[elements[0]] = FLIGHT(line)
                if len(out1[elements[0]].passenger_list()) == 1:    # If the flight only has 1 passenger
                    tmp = get_levenshtein_match(elements[0], out3)  # it is possibly an error
                    out1[tmp].PASSENGER_LIST.extend(FLIGHT(line).PASSENGER_LIST)
                    out1.pop(elements[0])

        
    return out1, out2, out3, out4


main()