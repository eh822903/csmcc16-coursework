VARIABLE                 (OBJECTIVE)
--------------------------------------------------------------
AIRPROT:

NUMBER OF FLIGHTS               (O1)

FLIGHTS:

FLIGHT ID                       (O2)

PASSENGER ID                    (O2)

IATA/FAA CODE                   (O2)

DEPARTURE TIME                  (O2)

ARRIVAL TIME                    (O2)

FLIGHT TIME                     (O2)

NUMBER OF PASSENGERS            (O3)

NAUTICAL MILE                   (O4)


PASSENGER:

TOTAL NAUTICAL MILE TRAVELLED   (O4)


AComp_Passenger_data.csv:
--------------------------------------------------------------
PASSENGER_ID        (XXXnnnnXXn)

FLIGHT_ID           (XXXnnnnX)

FROM_AIRPORT        (XXX)

TO_AIRPORT          (XXX)

DEPART_TIME         (n[10]) Unix epoch Time

TOTAL_FLIGHT_TIME   (n[1:4]) mins


Top30_airports_LatLong.csv:
--------------------------------------------------------------
AIRPORT_NAME        (X[3..20])

AIRPORT_CODE        (XXX)

AIRPORT_LAT         (n.n[3..13])

AIRPORT_LONG        (n.n[3..13])



DATA STRUCTURE FLIGHT:
--------------------------------------------------------------
(1) FLIGHT_ID

(2) FROM_AIRPORT

(3) TO_AIRPORT

(4) DEPARTURE_TIME

(5) FLIGHT_TIME

(6) PASSENGER_ID (LIST)

COMBINE (1) & (2) FOR THE MAPREDUCE KEY

